# Kite #
 OpenFrameworks application for interfacing with a Arduino Uno to create a rhythm looper. Currently only tested for Mac & Uno (roadmap includes Raspberry Pi support).

## Setup ##

* OpenFrameworks -- http://openframeworks.cc/

OF Addon Dependencies: 

* MSABPMTapper -- https://github.com/memo/ofxMSABPMTapper
* MSATimer -- https://github.com/memo/ofxMSATimer/

### Firmware ###

Using the Arduino IDE, flash an Arduino Uno with the `firmware/StandardFirmata-with-pullups.ino` firmware. This is just Standard Firmata that has been minimally tweaked to support pull ups.

## Pin List ##

* 2 -- Loop play on/off
* 3 -- Increment Loop Beat Length (max 8 beats) (Will cycle back to 1)
* 4 -- Clear Loop Recordings
* 5-8 -- Instrument Tap Buttons
* 11 -- PWM Metronome LED

* A0 -- Tempo Pot
* A2-A5 -- Digital LED Outs for instruments

## Author ##
* Hiron Roy (me@hironroy.com)