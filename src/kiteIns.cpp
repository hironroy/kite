//
//  kiteIns.cpp
//  kite
//
//  Created by Hiron Roy on 3/20/15.
//
//

#include "kiteIns.h"

namespace kite {
    Ins::Ins(){}
    
    Ins::Ins(ofArduino& ard, BeatTracker& beatTracker, int pin, int inputPin){
        _ard = &ard;
        _beatTracker = &beatTracker;
        _pin = pin;
        _inputPin = inputPin;
        
        _doingHit = false;
    }
    
    void Ins::setup(){
        _ard->sendDigitalPinMode(_inputPin, ARD_INPUT);
        _ard->sendDigitalPinMode(_pin, ARD_OUTPUT);
    }
    
    void Ins::update(){
        //Push the LED if within threshold
        
        if(!_ard->getDigital(_inputPin)){
            doHit();
        }
        else if(_beatTracker->isLooping()) {
            //check if there is a hit within the threshold
            float loopRatio = _beatTracker->getLoopRatioComplete();

            for( int i = 0; i < _taps.size(); i++){
                float hitRatio = _taps[i];
                //check if the hit is within threshold
                if( 0.01 >= abs(hitRatio - loopRatio)){
                    doHit();
                    break;
                }
            }
        }
        
        if(_doingHit){
            _ard->sendDigital(_pin, ARD_HIGH);
        }
        else{
            _ard->sendDigital(_pin, ARD_LOW);
        }
        
        if(_timer.getSeconds() > .1){
            _timer.stop();
            _doingHit = false;
        }
    }
    
    void Ins::clearTaps(){
        _taps.clear();
    }
    
    void Ins::doHit(){
        if(!_doingHit){
            float perComplete = _beatTracker->getLoopRatioComplete();
            _taps.push_back(perComplete);
            
            ofLogNotice()  << "Instrument Hits";
            for( int i = 0; i < _taps.size(); i++){
                ofLogNotice()  << _taps[i] << endl;
            }
            
            _timer.start();
            _doingHit = true;
        }
    }
    
    
    
}