#pragma once

#include "ofMain.h"
#include "ofEvents.h"
#include "MSABPMTapper.h"
#include "kiteBeatTracker.h"
#include "kiteMetronome.h"
#include "kiteIns.h"

using  namespace msa;
using namespace kite;

class ofApp : public ofBaseApp{
    
public:
    
    ofApp();
    
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    ofImage				bgImage;
    ofTrueTypeFont		font;
    ofTrueTypeFont      smallFont;
    ofArduino	ard;
    
    BPMTapper bpmTracker;
    BeatTracker _beatTracker;
    Metronome metronome;
    
    
    Ins ins1;
    Ins ins2;
    Ins ins3;
    Ins ins4;
    
    bool		bSetupArduino;			// flag variable for setting up arduino once
    
private:
    
    void setupArduino(const int & version);
    void digitalPinChanged(const int & pinNum);
    void analogPinChanged(const int & pinNum);
    void updateArduino();
    
    string buttonState;
    string potValue;
    
};

