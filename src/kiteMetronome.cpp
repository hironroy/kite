//
//  kiteMetronome.cpp
//  kite
//
//  Created by Hiron Roy on 3/20/15.
//
//

#include "kiteMetronome.h"

namespace kite {
    Metronome::Metronome(){}
    
    Metronome::Metronome(ofArduino& ard, BeatTracker& beatTracker, int pin, int potPin){
        _ard = &ard;
        _beatTracker = &beatTracker;
        _pin = pin;
        _potPin = potPin;
        
        _doingHit = false;
        
        _ledBrightness = 255;

    }
    
    void Metronome::setup(){
        _ard->sendAnalogPinReporting(_potPin, ARD_ANALOG);
        _ard->sendDigitalPinMode(_pin, ARD_PWM);
    }

    void Metronome::update(){
        //Check BPM setting and update
        float potVal = ((float)_ard->getAnalog(_potPin));
        float bpm = ( potVal / 1023.00) * 300;
        
         _beatTracker->setBpm(bpm);
        
        //Push the LED if within threshold
        float cos = _beatTracker->getBeatCos();
        
        if(cos >= 0.90){
            float loopCos = _beatTracker->getLoopCos();
            if(loopCos >= 0.90){
                _ledBrightness = 255;
            }
            else{
                _ledBrightness = 50;
            }
            doHit();
        }
        
        //LED Hits updating
        if(_doingHit){
            _ard->sendPwm(_pin, _ledBrightness);
        }
        else{
            _ard->sendPwm(_pin, 0);
        }
        
        if(_timer.getSeconds() > .1){
            _timer.stop();
            _doingHit = false;
        }
    }
    
    void Metronome::doHit(){
        if(!_doingHit){
            _timer.start();
            _doingHit = true;
        }
    }
    
}