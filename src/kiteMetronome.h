//
//  kiteMetronome.h
//  kite
//
//  Created by Hiron Roy on 3/20/15.
//
//

#ifndef __kite__kiteMetronome__
#define __kite__kiteMetronome__

#include "ofMain.h"
#include "MSABPMTapper.h"
#include "MSATimer.h"
#include "kiteBeatTracker.h"

using  namespace msa;

namespace kite {
    
    class Metronome {
    public:
        Metronome();
        Metronome(ofArduino& ard, BeatTracker& beatTracker, int pin, int potPin);
        void setup();
        void update();
        
        Timer _timer;
    protected:
        void doHit();
        bool _doingHit;

        ofArduino* _ard;
        BeatTracker* _beatTracker;
        
        int _pin;
        int _potPin;
        
        int _ledBrightness;

    };
}


#endif /* defined(__kite__kiteMetronome__) */
