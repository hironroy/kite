//
//  kiteIns.h
//  kite
//
//  Created by Hiron Roy on 3/20/15.
//
//

#ifndef __kite__kiteIns__
#define __kite__kiteIns__

#include "ofMain.h"
#include "ofEvents.h"

#include "MSABPMTapper.h"
#include "MSATimer.h"
#include "kiteBeatTracker.h"


using  namespace msa;

namespace kite {
    
    class Ins {
    public:
        Ins();
        Ins(ofArduino& ard, BeatTracker& beatTracker, int pin, int inputPin);
        void setup();
        void update();
        void clearTaps();
    protected:
        ofArduino* _ard;

        BeatTracker* _beatTracker;
        Timer _timer;

        int _inputPin;
        int _pin;
        bool _doingHit;
        
        void doHit();
        
        vector<float> _taps;  // create an empty vector of floats
    };
}

#endif /* defined(__kite__kiteIns__) */
