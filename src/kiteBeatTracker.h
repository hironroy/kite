//
//  kiteBeatTracker.h
//  kite
//
//  Created by Hiron Roy on 3/22/15.
//
//

#ifndef __kite__kiteBeatTracker__
#define __kite__kiteBeatTracker__

#include "ofMain.h"
#include "MSABPMTapper.h"
#include <math.h>

using  namespace msa;

namespace kite {
    class BeatTracker {
    public:
        BeatTracker();
        BeatTracker(BPMTapper& bpmTracker, int maxLoopBeats);
        
        float getBeatCos();
        float getLoopCos();
        
        bool isLooping();
        void toggleLooping();
        
        void incrementBeatCount();
        void setBpm(float bpm);
        
        float getLoopRatioComplete();
        
        void update();
    protected:
        
        BPMTapper* _bpmTracker;
        int _beatCount;
        bool _looperOn;
        int _maxLoopBeats;
    };
}

#endif /* defined(__kite__kiteBeatTracker__) */
