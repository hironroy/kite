//
//  kiteBeatTracker.cpp
//  kite
//
//  Created by Hiron Roy on 3/22/15.
//
//

#include "kiteBeatTracker.h"

namespace kite {
    BeatTracker::BeatTracker(){
    }
    
    BeatTracker::BeatTracker(BPMTapper& bpmTracker, int maxLoopBeats){
        _bpmTracker = &bpmTracker;
        _maxLoopBeats = maxLoopBeats;
        _beatCount = 3;
    }
    
    
    float BeatTracker::getBeatCos(){
        if(!_looperOn){
            return 1;
        }
        else{
            return _bpmTracker->beatCosS(1);
        }
    }
    
    float BeatTracker::getLoopCos(){
        if(!_looperOn){
            return 0;
        }
        else{
            return _bpmTracker->beatCosS(_beatCount);
        }
    }
    
    float BeatTracker::getLoopRatioComplete(){
        if(!_looperOn){
            return 0;
        }
        else{
            //percentage of the current beat complete
            float beatStartTime = _bpmTracker->beatStartTime();
            
            float lengthOfBeat = (60.0) / _bpmTracker->bpm();
            float lengthOfLoop = _beatCount * lengthOfBeat;
            
            float lengthIntoLoop = fmod(beatStartTime, lengthOfLoop);
            float ratio = lengthIntoLoop / lengthOfLoop;

            return ratio;
        }
    }
    
    void BeatTracker::incrementBeatCount(){
        if(_beatCount < _maxLoopBeats){
            _beatCount++;
        }
        else{
            _beatCount = 1;
        }
        ofLogNotice() << "Loop Beat Length " + ofToString(_beatCount);
    }
    
    void BeatTracker::setBpm(float bpm){
        if( abs(_bpmTracker->bpm() - bpm) > 2){
            _bpmTracker->setBpm(bpm);
        }
    }
    
    void BeatTracker::toggleLooping(){
        _looperOn = !_looperOn;
        _bpmTracker->startFresh();
        ofLogNotice() << "Looper ON/OFF " + ofToString(_looperOn);
    }
    
    bool BeatTracker::isLooping(){
        return _looperOn;
    }

}
